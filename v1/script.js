
// Element selectors
const factList = document.querySelector('.fact-list');

document.addEventListener('DOMContentLoaded', function() {
  console.log('The DOM is ready!');
});

// Load data from API
async function getFacts() {
    const res = await fetch("https://qerbpucisnizrbswkgfl.supabase.co/rest/v1/facts", {
        headers: {
            apikey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InFlcmJwdWNpc25penJic3drZ2ZsIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDM5NjYyNjcsImV4cCI6MjAxOTU0MjI2N30.7ER-bDXO1_ECnbWRCU0zpacCbK5lXSM1y7hOUGgxHOA",
            authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InFlcmJwdWNpc25penJic3drZ2ZsIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDM5NjYyNjcsImV4cCI6MjAxOTU0MjI2N30.7ER-bDXO1_ECnbWRCU0zpacCbK5lXSM1y7hOUGgxHOA"
        }
    });
    const data = await res.json();
    console.log(data);
    const newFacts = data.filter(fact => fact.category === 'history');
    console.log(newFacts);
    return data;
}

const res = getFacts();

// Event listeners
document.querySelector('.btn-open').addEventListener('click', function() {
    document.querySelector('.fact-form').classList.toggle('hidden');
    this.textContent = this.textContent === 'Close' ? 'Share a Fact' : 'Close';
    factList.innerHTML = '';
});


// Functions
const calcFactAge = (fact) => {
    const factDate = new Date(fact.created_at);
    const now = new Date();
    const diff = now - factDate;
    const age = Math.max(Math.floor(diff / (1000 * 60 * 60 * 24 * 365)), 0);
    return age;
};
