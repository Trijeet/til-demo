
import './App.css';
import { useEffect, useState } from 'react';
import supabase from './supabase';

const CATEGORIES = [
  { name: "technology", color: "#3b82f6" },
  { name: "science", color: "#16a34a" },
  { name: "finance", color: "#ef4444" },
  { name: "society", color: "#eab308" },
  { name: "entertainment", color: "#db2777" },
  { name: "health", color: "#14b8a6" },
  { name: "history", color: "#f97316" },
  { name: "news", color: "#8b5cf6" },
];

function isValidHttpUrl(string) {
  let url;
  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }
  return url.protocol === 'http:' || url.protocol === 'https:';
}

const Loader = () => {
  return (
    <p className="message">Loading...</p>
  );
}

const App = () => {
  const [showForm, setShowForm] = useState(false);
  const [facts, setFacts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [currentCategory, setCurrentCategory] = useState("all");

  useEffect(
    () => {
      async function getFacts() {

        let query = supabase.from('facts').select("*");
        if (currentCategory !== "all") {
          query = query.eq("category", currentCategory);
        }

        setIsLoading(true);
        const { data: facts, error } = await query
          .order("vote_interesting", { ascending: false })
          .limit(100);
        setIsLoading(false);
        if (error) {
          alert('There was a problem fetching facts');
        } else {
          setFacts(facts);
        }
      }
      const facts = getFacts();
    },
    [setFacts, currentCategory]
  );

  return (
    <div>
      
      <Header showForm={showForm} setShowForm={setShowForm} />

      {/* <Counter /> */}
      {showForm ? <NewFactForm setFacts={setFacts} setShowForm={setShowForm}/> : null}
      
      <main className="main">
        <CategoryFilter setCurrentCategory={setCurrentCategory} />
        {isLoading ? <Loader /> : <FactList facts={facts} setFacts={setFacts} />}
      </main>
      
    </div>
  );
};

const Header = ({showForm, setShowForm}) => {
  return (
    <header className="header">
          <div className="logo">
            <img src="logo.png" alt="Today I Learned Logo" srcSet="" />
            <h1>Today I Learned</h1>
          </div>
          <button className="btn btn-open" onClick={()=>setShowForm(!showForm)}>
            {showForm ? "Close" : "Share a Fact"}
          </button>
      </header>
  );
}

const NewFactForm = ({ setFacts, setShowForm }) => {

  const [text, setText] = useState("");
  const [source, setSource] = useState("");
  const [category, setCategory] = useState("");
  const [isUploading, setIsUploading] = useState(false);
  const textLimit = text.length;

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (text && isValidHttpUrl(source) && category && (textLimit <= 200)) {
      console.log("Fact added");
      
      setIsUploading(true);
      const { data: newFact, error } = await supabase
          .from('facts')
          .insert([{ fact: text, source, category }])
          .select();
      setIsUploading(false);
      if (error) {
        alert('There was a problem adding your fact');
      } else {
        setFacts((facts) => [newFact[0], ...facts]);
        setText("");
        setSource("");
        setCategory("");
        setShowForm(false);
      }
    }
  }

  return (
    <div>
      <form action="/" method="post" className="fact-form" onSubmit={handleSubmit}>
          <input 
            type="text"
            name="fact"
            id="title"
            placeholder="Share a fact with the world..."
            value={text}
            onChange={(e)=>setText(e.target.value)}
            disabled={isUploading}
          />
          <span className='char-counter'>{200 - textLimit}</span>
          <input 
            type="text"
            name="source"
            id="source"
            placeholder="Trustworthy source"
            value={source}
            onChange={(e)=>setSource(e.target.value)}
            disabled={isUploading}
          />
          <select 
            name="category"
            id="category"
            value={category}
            onChange={(e)=>setCategory(e.target.value)}
            disabled={isUploading}
          >
              <option value="">Select a category</option>
              {CATEGORIES.map((e) => <option key={e.name} value={e.name}>{e.name.toLocaleUpperCase()}</option>)}
          </select>
          <input type="submit" className="btn btn-large" value="Add Fact"></input>
      </form>
    </div>
  );
}

const Counter = () => {

  const [count, setCount] = useState(50);

  return (
    <div className="counter">
      <button type="button" className="btn btn-counter btn-large" onClick={
        ()=>setCount(count+1)}>
        👍
      </button>
      <span style={{fontSize: "40px", color: "white"}}>{count}</span>
    </div>
  );
}

const Fact = ({fact_data, setFacts}) => {

  const [isUpdating, setIsUpdating] = useState(false);
  let {fact, source, category, vote_interesting, vote_mindblowing, vote_false} = fact_data;
  let isInteresting = vote_interesting > vote_mindblowing && vote_interesting > vote_false;
  let isMindBlowing = vote_mindblowing > vote_interesting && vote_mindblowing > vote_false;
  let isDisputed = vote_false > vote_interesting && vote_false > vote_mindblowing;

  const handleVote = async (e) => {
    const vote = e.target.name;
    setIsUpdating(true);
    const { data: updatedFact, error } = await supabase
      .from('facts')
      .update({ [vote]: fact_data[vote] + 1 })
      .eq('id', fact_data.id)
      .select();
    if (error) {
      alert('There was a problem updating the fact');
    } else {
      setFacts((facts) => facts.map((f) => f.id === updatedFact[0].id ? updatedFact[0] : f));
      setIsUpdating(false);
    }
  }

  return (
    <li className="fact">
      <p>{fact}</p>
      <a href={source} target="_blank" className="srclink">
          (Source)
      </a>
      <span className="tag" style={{
        backgroundColor: CATEGORIES.find((i) => i.name===category).color
        }}
      >{category}</span>
      <div className="reactions">
          <button disabled={isUpdating} onClick={handleVote} name="vote_interesting">👍{vote_interesting}</button>
          <button disabled={isUpdating} onClick={handleVote} name="vote_mindblowing">🤯{vote_mindblowing}</button>
          <button disabled={isUpdating} onClick={handleVote} name="vote_false">😒{vote_false}</button>
      </div>
    </li>
  )
};

const FactList = ({ facts, setFacts }) => {

  if (facts.length === 0) {
    return (
      <p className="message">No facts to display.</p>
    );
  }

  return (
  <section>
    <ul className="fact-list">
      {facts.map((e) => <Fact key={e.id} fact_data={e} setFacts={setFacts} />)}
    </ul>
    <p>There are {facts.length} facts in the database. Add your own!</p>
  </section>
  )
};

const Category = ({cat, setCurrentCategory}) => {
  return (
    <li className="category">
      <button 
        type="button"
        className="btn btn-category"
        style={{backgroundColor: cat.color}}
        onClick={()=>setCurrentCategory(cat.name)}
      >{cat.name}</button>
    </li>
  );
}

const CategoryFilter = ({ setCurrentCategory }) => {
  return (
    <div className="category-filter">
      <aside>
          <ul>
              <li className="btn btn-all btn-category">
                <button type="button" className="btn-all" onClick={()=>setCurrentCategory("all")}>all</button>
              </li>
              {CATEGORIES.map((e) => <Category key={e.name} cat={e} setCurrentCategory={setCurrentCategory} />)}
          </ul>
      </aside>
    </div>
  );
}

export default App;
